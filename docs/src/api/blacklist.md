# 🛑 Blacklist
If you're making something that requires a check on someone's status within a divison (Like being blacklisted), then you can use this API endpoint. Use the following parameters to check someone's blackist status:  

## Required to request 

**URL**: ``https://pb-kronos.dev/api/<division>/blacklist/checkusers?userids=<UserIDs>`` (See below for more details)  
**Possible URL Arguments**:  
- ``?userids=``
  * ``28462``
  * ``28462,27263812``
  * ``46367,3837293,23464``
**Possible Parameters**:   
- ``<divisions>``:  
    - **PBST**  
    - **TMS**  

**Required Method**: ``GET``  
**Required Headers**:    
```json
Headers = {
  ["Access-Key"] = "<YOUR TOKEN HERE>",
  ["Content-Type"] = "application/json"
}
```

## Expected Response 
```json
{
   "<UserID>": BOOLEAN,
   "<UserID>": BOOLEAN
}
```

## Example Response  
```json
{
   "46367":true,
   "346745":false,
   "485783":true,
   "3423567":false,
   "224578":true
}
```  

## Possible Error Messages
``403``: *Invalid Permission / Invalid/Incorrect API Key*  
``429``: *Exceeded ratelimit*  
``501``: *Divison does not have an existing blacklist*  
``502``: *Invalid entry data*  
